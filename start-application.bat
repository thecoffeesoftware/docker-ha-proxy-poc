call mvn package
call docker network create --driver bridge docker-proxy-poc || true
call docker build -t docker-proxy-poc-server-1 ./docker
call docker build -t docker-proxy-poc-server-2 ./docker
call docker build -t docker-proxy-poc-haproxy -f ./docker/haproxy ./docker
call docker run -d --network docker-proxy-poc --name docker-proxy-poc-server-1-container --net-alias proxypochost1 docker-proxy-poc-server-1 --server.port=9000 --inside-url=proxypochost2:9001/api/two
call docker run -d --network docker-proxy-poc --name docker-proxy-poc-server-2-container --net-alias proxypochost2 docker-proxy-poc-server-2 --server.port=9001 --inside-url=proxypochost1:9000/api/one
call docker run -d --network docker-proxy-poc --name docker-proxy-poc-haproxy-container -p 9002:9002 docker-proxy-poc-haproxy
