package com.thecoffee.dockerpoc.restresources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api")
public class TestResource {

	private final InsideService insideService;

	@Autowired
	public TestResource(InsideService insideService) {
		this.insideService = insideService;
	}

	@RequestMapping(value = "one")
	public String getOne() {
		return "one";
	}

	@RequestMapping(value = "two")
	public String getTwo() {
		return "two";
	}

	@RequestMapping(value = "one/count")
	public String getOneCount() {
		return Integer.toString(insideService.getCount());
	}

	@RequestMapping(value = "two/count")
	public String getTwoCount() {
		return Integer.toString(insideService.getCount());
	}
}
