package com.thecoffee.dockerpoc.restresources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class InsideService {

	private final Logger logger = LoggerFactory.getLogger(InsideService.class);

	private final ConfigurationService configurationService;

	public static final AtomicInteger counter = new AtomicInteger(0);

	@Autowired
	public InsideService(ConfigurationService configurationService) {
		this.configurationService = configurationService;
	}

	@Scheduled(fixedDelay = 2000)
	public void getInside() {
		try {
			String inside = configurationService.restTemplate().getForObject("http://" +configurationService.insideUrl.concat("/count"), String.class);
			if (inside != null) {
				counter.incrementAndGet();
			}
		} catch (Exception e) {
			logger.info("Could not reach inside", e);
		}
	}

	public int getCount() {
		return counter.get();
	}
}
