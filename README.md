
**preconditions**

    1. java installed on your computer
    2. maven installed on your computer
    3. docker installed on your computer
**use**

    1. open a terminal from the root directory of the project
    2. execute "start-application"
    3. try to send get http requests to localhost:9002 and localhost:9002/count
**clean up**

    1. open a terminal from the root directory of the project
    2. execute "cleanup"
**info**  
Container port 9000 and 9001:  
_In an ideal case this port shifting is not necessary,  
because both server runs in different containers which means they have different hosts._

Network alias (host for a container on a network):  
_If the host of the containers are the same, HA proxy will not be able to resolve the container behind the given host  
So we have to specify different host for each server._

_Some version of HA proxy checks address in parse time, so it fails to start if we specify a container id or name as a host address.  
So that we have to give a network alias like an ip address (or other valid host address) as hostname for both of our server containers._

Expose doesn't necessary in dockerfile:  
_The EXPOSE instruction does not actually publish the port.  
It functions as a type of documentation between the person who builds the image and the person who runs the container, about which ports are intended to be published.  
To actually publish the port when running the container, use the -p flag on docker run to publish and map one or more ports, or the -P flag to publish all exposed ports and map them to high-order ports._

HA Proxy:  
_Just for POC purposes HA proxy is configured to send any incoming request on port 9002 to itself  
There are two proxy fronted on different ports to receive requests from HA proxy itself which are configured to send requests to different backend servers  
With this configuration we can modify Path in the url depending on which server won the load balancing  
Each stages of the proxy set information in headers about the proxy flow  
Note that only the 9002 port is published so from outside nor the proxy 9000 and 9001 ports nor the backend server 9000 and 9001 ports are available_

Get logs for exited container in case of any error:   
_for logs -> execute in terminal "docker logs -t docker-proxy-poc-haproxy-container"  
for network info -> execute in terminal "docker inspect docker-proxy-poc-haproxy-container"  
for bash -> execute in terminal "docker exec -it docker-proxy-poc-server-1-container bash"  
(you can call curl to check network availability for containers)  
On windows you can check any information at the Dashboard menu of the docker desktop application_